<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package autoexpo
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found wrap940 text-center">
				<header class="page-header">
					<h1 class="page-title" style="font-size: 300px;margin: 0;line-height: 1;padding-top: 100px;">404</h1>
					<p>Parece que algo no salio bien.</p>
				</header><!-- .page-header -->
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
