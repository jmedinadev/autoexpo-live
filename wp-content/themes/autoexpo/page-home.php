<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

/* Template Name: Inicio */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'home' );
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
?>

<script>
///////////// MODAL//////////////////
(function($){
	// console.log("entro");
	$(document).on("click", "#myBtn2", function(e){
		e.preventDefault();
		$("#myModal").css("display", "block");
		$("#bone").get(0).play();
	});

	$(document).on("click", "#close", function(e){
		$("#myModal").css("display", "none");
		$("#bone").get(0).pause();
	});

	// When the user clicks anywhere outside of the modal, close it

	// Culpable: jonay medina
	// Correcion de error con el window.onclick
	// NEW
	$("body").click(function(){
        $("#myModal").css("display", "none");
		$("#bone").get(0).pause();
    });

    // OLD
	// window.onclick = function(event) {
	// 	if (event.target == modal) {
	// 		$("#myModal").css("display", "none");
	// 		$("#bone").get(0).pause();
	// 	}
	// }
})(jQuery);
</script>
