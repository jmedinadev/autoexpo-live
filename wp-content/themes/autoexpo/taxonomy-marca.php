<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

get_header();

$sector = get_queried_object()->slug;
// var_dump($sector);
$vehiculos = get_vehiculos($sector);
$marcas = get_all_taxonomies('marca');
$categorias = obtener_categorias_casas($sector)['categorias'];
$casa_matriz = obtener_categorias_casas($sector)['casamatriz'];
$filtros_adicionales = get_all_taxonomies('filtros_adicionales');
// var_dump($categorias);

$max = get_max_price();
$min = get_min_price();

// var_dump($cars);
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header wrap940 text-center">
				<h1 class="page-title"><?php the_archive_title();
				?></h1>
			</header><!-- .page-header -->
			<input type="hidden" name="currentpage" value="1">
			<div id="catalogo" class="catalog catalog--brand mPS2id-target">
				<div class="catalog__header">
					<div class="catalog__wrap wrap940">
						<form>
							<div class="opciones">
								<h2>Catálogo</h2>
								<div class="precios">
									<div class="radios">
										<span>Rango de precios</span>
										<label for="dolar">
											<input id="dolar" type="radio" name="moneda" checked>
											<span class="moneda" data-moneda="$">$</span>
										</label>
										<label for="quetzal">
											<input id="quetzal" type="radio" name="moneda">
											<span class="moneda" data-moneda="Q">Q</span>
										</label>
									</div>
									<div class="slider">
										<fieldset class="filter-price">
											<div class="filter-price-content">

												<div class="price-wrap">
													<div class="price-wrap-1">
														<label for="one"></label>
														<input id="one" readonly>
													</div>
												</div>


												<div class="price-field">
													<input type="range" class="range" min="<?php echo $min; ?>" max="<?php echo $max; ?>" id="lower" value="<?php echo $min; ?>">
													<input type="range" class="range" min="<?php echo $min; ?>" max="<?php echo $max; ?>" id="upper" value="<?php echo $max; ?>">
												</div>


												<div class="price-wrap">
													<div class="price-wrap-2">
														<label for="two"></label>
														<input id="two" readonly>
													</div>
												</div>
											</div>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="dropdowns">
								<select name="marca" id="marca" disabled>
									<option disabled selected>Escoge una marca</option>
									<?php foreach ($marcas as $key => $value) {
											if($sector == $value->slug){
												?>
													<option value="<?php echo $value->slug; ?>" selected ><?php echo $value->name; ?></option>
												<?php
											} else {
												?>
													<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
												<?php
											}
										}
									?>
								</select>
								<select name="filtros_adicionales" id="filtros_adicionales">
									<option value="">Ver todos</option>
									<?php foreach ($filtros_adicionales as $key => $value) { ?>
										<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
									<?php } ?>
								</select>
								<select name="categoria" id="categoria">
									<option disabled selected>Categoría</option>
									<?php foreach ($categorias as $key => $value) { ?>
										<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
									<?php } ?>
								</select>
								<select name="casamatriz" id="casamatriz">
									<option disabled selected>Casa Matríz</option>
									<?php foreach ($casa_matriz as $key => $value) { ?>
										<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
									<?php } ?>
								</select>
							</div>
						</form>
					</div>
				</div>
				<div class="catalog__content">
					<div class="catalog__vehicles">
						<div class="catalog__wrap-items wrap940">
							<?php foreach($vehiculos as $item): ?>
							<div class="catalog__item">
								<div class="catalog__item-header">
									<?php if(!empty($item->vista_virtual)): ?>
										<div class="catalog__vista360">
											<img src="<?php bloginfo('template_url'); ?>/images/icon-360.png" alt="Vista 360">
										</div>
									<?php endif; ?>
									<?php if(!empty($item->promo)): ?>
									<div class="catalog__item-header-promo">AUTOEXPO 2020</div>
									<?php endif; ?>
									<a href="<?php echo $item->link; ?>">
										<img src="<?php echo $item->images['thumbnail'][0] ?>" alt="">
									</a>
								</div>
								<div class="catalog__item-body">
									<?php if(empty($item->titulo_corto)): ?>
										<h3><a href="<?php echo $item->link; ?>"><?php echo $item->post_title; ?></a></h3>
									<?php else: ?>
										<h3><a href="<?php echo $item->link; ?>"><?php echo $item->titulo_corto; ?></a></h3>
									<?php endif; ?>
									<div class="price">
										<span><?php echo $item->marca; ?></span>
										<span><?php if($item->moneda == 'Quetzales'): ?>Q<?php else:?>$<?php endif;?> <?php echo $item->precio ?></span>
									</div>
									<div class="buttons">
										<!-- Si el vehiculo se guarda en mi garage agregar esta clase al siguiente link 'my-garage' -->
										<a href="javascript:void(0)" class="add-garage <?php if($item->added){ echo "my-garage";} ?>" data-id="<?php echo $item->ID ?>"><span class="icon-timon"></span> A favoritos</a>
										<a href="<?php echo $item->link; ?>">Ver detalle</a>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="text-center mtop80">
							<h2 class="no-more hide">No hay más vehículos</h2>
							<a href="javascript:void(0)" class="cta more-vehicles">Ver más</a>
						</div>

					</div>
					<!-- Vehicles -->
					<div id="marcas" class="catalog__partners wrap940 mPS2id-target">
						<h2 class="title-section">Marcas participantes</h2>
						<div class="catalog__wrap-partners wrap940">
							<ul class="slide-partners">
							<?php
								$terms = get_terms( array(
									'taxonomy' => 'marca',
									'parent'   => 0
								) );
								foreach($terms as $brand):
								$brand_image = get_field('logotipo_de_la_marca', 'term_'.$brand->term_id);

								$brand_link = get_term_link($brand);
							?>
								<li>
									<div class="partner__item">
										<a href="<?php echo $brand_link; ?>"></a>
										<img src="<?php echo $brand_image['sizes']['thumbnail']; ?>" alt="<?php echo $brand_image['alt']; ?>" width="<?php echo $brand_image['width']; ?>" height="<?php echo $brand_image['height']; ?>">
									</div>
								</li>
							<?php endforeach; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- Catalog -->

			<?php endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
