<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package autoexpo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo('template_url'); ?>/images/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://solicitudes.baccredomatic.com/web-determinations/staticresource/interviews.css" />
	<link rel="stylesheet" href="https://solicitudes.baccredomatic.com/web-determinations/staticresource/fonts/fonts.css" />
	<link rel="stylesheet" href="https://archivos.baccredomatic.com/opa/css/spinner.min.css" />
	<link href="https://archivos.baccredomatic.com/opa/css/opa.prod.min.css?v=4.0" rel="stylesheet">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/slick-theme.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/slick.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/custom.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/icons.css">
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div class="content-btn-float test-drive">
	<a class="btn-float" href="/test-drive">Agendar Test Drive</a>
</div>
<div class="content-btn-float">
	<a class="btn-float" href="/contacto">Solicita tu préstamo</a>
</div>
<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
<div id="page" class="site site-tests">
	<header id="masthead" class="header no-print">
		<div class="header__wrap header-mobile">
			<div class="header-mobile__top">
				<div class="branding">
					<a href="<?php echo get_home_url(); ?>" class="custom-logo-link" rel="home"><img width="84" height="29" src="<?php echo get_home_url();?>/wp-content/uploads/2019/06/Bac_credomatic_logo.png" class="custom-logo" alt="Auto Expo"></a>
				</div><!-- .site-branding -->
			</div>
			<div class="header-mobile__bottom">
				<nav id="navigation-mobile" class="main-navigation">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-2',
							'menu_id'        => 'mobile-menu',
						) );
					?>
				</nav><!-- #site-navigation -->
				<ul>
					<li id="menu-item-21" class="menu-item-21"><a data-ps2id-api="true">Mis favoritos<span class="icon-timon"></span></a></li>
				</ul>
			</div>
		</div>
		<!-- Movil -->
		<div class="header__wrap wrap940 header-web">
			<div class="branding">
				<a href="<?php echo get_home_url(); ?>" class="custom-logo-link" rel="home"><img width="84" height="29" src="<?php echo get_home_url();?>/wp-content/uploads/2019/06/Bac_credomatic_logo.png" class="custom-logo" alt="Auto Expo"></a>
			</div><!-- .site-branding -->

			<nav id="navigation" class="main-navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
				?>
			</nav><!-- #site-navigation -->
		</div>
		<!-- Web -->
	</header><!-- #masthead -->

	<div class="my-cars">
		<div class="my-cars__wrap">
			<div class="my-cars__close">
				<span class="close-my-cars">Cerrar  <span class="icon-cross"></span></span>
			</div>
			<h3>Mis Favoritos</h3>
			<div class="my-cars__cars">
				<?php

					$my_vehicles = str_replace('[',"",$_COOKIE["garage"]);
					$my_vehicles = str_replace(']',"",$my_vehicles);
					$my_vehicles = explode(',',$my_vehicles);

					if(reset($my_vehicles) != "" && count($my_vehicles) > 0 ){
						foreach ($my_vehicles as $key => $value) {
							$my_vehicle = get_vehicle_by_id($value);
							?>
							<div class="my-cars__item">
								<div class="my-cars__item--header">
									<!-- hay un tamaño creado para esta imagen que se llama 'thumb-favs' lo podes ver en el functions.php -->
									<a href="<?php echo $my_vehicle->link; ?>">
										<img src="<?php echo $my_vehicle->images["thumb-favs"][0] ?>" alt="<?php echo $my_vehicle->post_title ?>">
									</a>
								</div>
								<div class="my-cars__item--body">
									<?php if(empty($my_vehicle->titulo_corto)): ?>
										<h3><a href="<?php echo $my_vehicle->link; ?>"><?php echo $my_vehicle->post_title; ?></a></h3>
									<?php else : ?>
										<h3><a href="<?php echo $my_vehicle->link; ?>"><?php echo $my_vehicle->titulo_corto; ?></a></h3>
									<?php endif; ?>
									<span><?php echo $my_vehicle->marca; ?></span>
									<span><?php if( $my_vehicle->moneda == "Quetzales"){ echo "Q"; }else{ echo "$";} echo $my_vehicle->precio; ?></span>
								</div>
								<div class="my-cars__item--footer">
									<a href="javascript:void(0)" class="remove" data-id="<?php echo $my_vehicle->ID ?>">Remover</a>
									<a href="/contacto"  class="red close-my-cars">Solicitar préstamo</a>
								</div>
							</div>
						<?php }
					}else{
						?>
						<h4 class="error-garage">Aún no tienes favoritos.</h4>
						<?php
					}
				?>

			</div>
		</div>
	</div>

	<div id="content" class="site-content">
