<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package autoexpo
 */

?>

	</div><!-- #content -->

	<div class="modal hide">
		<div class="modal__content">
			<span class="js-close-modal">Cerrar <span class="icon-cross"></span></span>
			<h3>¡Agregaste un auto a tus favoritos!</h3>
			<a href="<?php echo get_home_url(); ?>#catalogo" class="cta js-close-modal">Regresar al catálogo</a>
		</div>
	</div>
	<!-- Modal -->

	<footer id="colophon" class="footer no-print">
		<div class="footer__wrap wrap940">
			<div class="copy">BAC Credomatic 2019 - All rights reserved</div>
			<div class="by">
				<a class="copy" style="text-decoration: none;" href="http://www.milkncookies.tv/" target="_blank">Powered by &nbsp<img style="display: inline-block; width: 25px;" src="<?php bloginfo('template_url'); ?>/images/poweredby.png" alt="MnC">&nbsp Tibal MnC</a>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<!-- <div class="over-responsive-menu"></div> -->

<?php wp_footer(); ?>
<script src="<?php bloginfo('template_url') ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/slick.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/site.js?v=1.1"></script>
<script>
	$(document).ready(function() {
		$('#menu-item-21 a').on('click', function(){
			$('.my-cars').fadeIn('slow');
			$('.my-cars__wrap').addClass('my-cars__wrap--open');
		});

		$('.close-my-cars').on('click', function(){
			$('.my-cars').fadeOut('slow');
			$('.my-cars__wrap').removeClass('my-cars__wrap--open');
		});

		$('.js-close-modal').on('click', function(){
			$('.modal').addClass('hide');
		});

		$('.slide-cars').slick({
		  infinite: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  dots: true,
		  autoplay: true
		});

		$('.slide-partners').slick({
		  infinite: true,
		  slidesToShow: 6,
		  slidesToScroll: 1,
		  arrows: true,
		  dots: false,
		  autoplay: true,
		  centerMode: true,
		  centerPadding: '35px',
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 760,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
		        dots: false,
		        infinite: true
		      }
		    }
		  ]
		});

		$('.slide-mobile').slick({
		  infinite: false,
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  arrows: false,
		  dots: false,
		  autoplay: false,
		});

		$('.promo-wrap__item--link').on('click', function() {
			$(this).closest('.promo-wrap--div').find('.promo-wrap__item-container').toggleClass('promo-wrap__item-container--show');
			$(this).closest('.promo-wrap--div').find('.promo-wrap__item--link').toggleClass('open-link');
		});
	});
</script>
<!-- <script>
	var elelist = document.getElementsByTagName("input");
	for(var i = 0; i < elelist.length; i++){
		elelist[i].addEventListener("focus", function(){
			this.blur();
	    });
	}
	document.getElementById("myInput").addEventListener("focus", function(){
		this.focus();
	});
</script> -->

</body>
</html>
