<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

/* Template Name: Contacto */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main wrap940">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'contacto' );
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
?>

<script src="https://solicitudes.baccredomatic.com/web-determinations/staticresource/interviews.js"></script>

<script>
	var OPA_Settings = {
		container: '#opa-container',
		deploymentName: 'GUA_SolicitudAutoShow',
		webDeterminationsUrl: 'https://solicitudes.baccredomatic.com/web-determinations',
		spinner: false,
		dynamicUpload: true
	};
</script>

<script src="https://archivos.baccredomatic.com/opa/js/opa.prod.min.js"></script>