$(document).ready(function($){

	marcas = window.marcas_author;
	$('#marca-tabs > li.hide-if-no-js').toggle();
	$('#marcachecklist').children().toggle();

	for (var i = marcas.length - 1; i >= 0; i--) {
		var n = marcas[i].term_id;
		$('#marca-'+n).toggle();
	}
	
});