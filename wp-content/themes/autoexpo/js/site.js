$(document).ready(function($){


    /*
    *
    *    SEARCH VEHICULOS
    *
    */
    var promoPick = 0;
    function min_value($moneda){
        if (moneda === '$') {
            $('#lower').addClass('hide');
            $('#lower_dols').removeClass('hide');
            
            
            min = $('#lower_dols').val();

        } else {
            $('#lower_dols').addClass('hide');
            $('#lower').removeClass('hide');
            
            min = $('#lower').val();
        }
        document.querySelector('#one').value=formatNumber(min);
        return min;
    }

    function max_value($moneda){
        if (moneda === '$') {
            $('#upper').addClass('hide');
            $('#upper_dols').removeClass('hide');
            max = $('#upper_dols').val();

        } else {
            $('#upper_dols').addClass('hide');
            $('#upper').removeClass('hide');
            
            max = $('#upper').val();
        }
        document.querySelector('#two').value=formatNumber(max);
        return max;
    }

    // $(".catalog__video").css("cursor", "pointer");
    // $(".catalog__video").on("click", function(){
    //     $('[name=filtros_adicionales]').val( 'promo' ).change();

    //     $('html, body').animate({
    //         scrollTop: $("#catalogo").offset().top
    //     }, 2000);
    // });

    //BY PROMO
    $(".filter-promo").on("click", function(e){
        e.preventDefault();
        $('[name=filtros_adicionales]').val( 'con-promocion' ).change();

        // $('html, body').animate({
        //     scrollTop: $("#catalogo").offset().top
        // }, 2000);
    });

    //culpable Jonay medina
    // BY PICK-UP
    $(".filter-pickup").on("click", function(e){
        e.preventDefault();
        promoPick = 1;
        $('[name=categoria]').val( 'pick-up' ).change();

        
    }); // cualquier error discord: jonay.medina
    
    //BY MONEDA
    $('.moneda').on("click", function(){

        $('.no-more').addClass('hide');
        moneda = $(this).data('moneda');
        console.log(moneda);
        min = min_value(moneda);
        max = max_value(moneda);
        marca = $('select[name="marca"]').val();
        categoria = $('select[name="categoria"]').val();
        casamatriz = $('select[name="casamatriz"]').val();
        promo360 = $('select[name="filtros_adicionales"]').val();
        
        search = $('#myInput').val();

        $('.moneda').removeClass('active');
        $(this).addClass('active');
        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    });

    // $('#myInput').on("change", function(){
    //     $('.no-more').addClass('hide');
    //     moneda = $('.moneda.active').data('moneda');;
    //     marca = $('select[name="marca"]').val();
    //     categoria = $('select[name="categoria"]').val();
    //     casamatriz = $('select[name="casamatriz"]').val();
    //     promo360 = $('select[name="filtros_adicionales"]').val();
    //     max = $('#upper').val();
    //     min = $('#lower').val();
    //     search = $('#myInput').val();

    //     getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    // });


    $('#myInput').bind("enterKey",function(e){
        console.log('my_input');
        $('.no-more').addClass('hide');
        moneda = $('.moneda.active').data('moneda');;
        marca = $('select[name="marca"]').val();
        categoria = $('select[name="categoria"]').val();
        casamatriz = $('select[name="casamatriz"]').val();
        promo360 = $('select[name="filtros_adicionales"]').val();
        min = min_value(moneda);
        max = max_value(moneda);
        search = $('#myInput').val();
        
        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    });

    $('#myInput').keyup(function(e){
        if(e.keyCode == 13){
            $(this).trigger("enterKey");
            $(".autocomplete-items").remove();
            return;
        }
        // console.log(e.keyCode);
        if(e.keyCode == 8){
            if($(this).val() == ""){
                $(this).trigger("enterKey");
                $(".autocomplete-items").remove();
            }
        }
    });

    $('#myInputautocomplete-list div').on("click", function(){
        console.log($('#myInput').val());
        $('#myInput').trigger("enterKey");
        $(".autocomplete-items").remove();
    });


    // $('#myInput').on("change", function(){
    //     $('.no-more').addClass('hide');
    //     moneda = $('.moneda.active').data('moneda');;
    //     marca = $('select[name="marca"]').val();
    //     categoria = $('select[name="categoria"]').val();
    //     casamatriz = $('select[name="casamatriz"]').val();
    //     promo360 = $('select[name="filtros_adicionales"]').val();
    //     max = $('#upper').val();
    //     min = $('#lower').val();
    //     search = $('#myInput').val();

    //     getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    // });


    // // MIN
    // $('#lower').on("change", function(){
    //     $('.no-more').addClass('hide');
    //     moneda = $('.moneda.active').data('moneda');;
    //     marca = $('select[name="marca"]').val();
    //     categoria = $('select[name="categoria"]').val();
    //     casamatriz = $('select[name="casamatriz"]').val();
    //     promo360 = $('select[name="filtros_adicionales"]').val();
    //     max = $('#upper').val();
    //     min = $(this).val();
    //     search = $('#myInput').val();

    //     getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    // });

    // // MAX
    // $('#upper').on("change", function(){
    //     $('.no-more').addClass('hide');
    //     moneda = $('.moneda.active').data('moneda');;
    //     marca = $('select[name="marca"]').val();
    //     categoria = $('select[name="categoria"]').val();
    //     casamatriz = $('select[name="casamatriz"]').val();
    //     promo360 = $('select[name="filtros_adicionales"]').val();
    //     max = $(this).val();
    //     min = $('#lower').val();
    //     search = $('#myInput').val();

    //     getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    // });

    //BY MARCA
    $('select[name="marca"]').on("change", function(){
        console.log('marca');
        $('.no-more').addClass('hide');
        moneda = $('.moneda.active').data('moneda');
        marca = $(this).val();
        // console.log(marca);
        categoria = $('select[name="categoria"]').val();
        casamatriz = $('select[name="casamatriz"]').val();
        promo360 = $('select[name="filtros_adicionales"]').val();
        min = min_value(moneda);
        max = max_value(moneda);
        search = $('#myInput').val();

        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
        getMarcasAjax(marca);
    });

    //BY CTEGORIA
    $('select[name="categoria"]').on("change", function(){
        console.log('categoria');
        $('.no-more').addClass('hide');
        moneda = '';
        marca = $('select[name="marca"]').val();
        categoria = $(this).val();
        casamatriz = $('select[name="casamatriz"]').val();
        promo360 = $('select[name="filtros_adicionales"]').val();
        min = min_value(moneda);
        max = max_value(moneda);
        search = $('#myInput').val();
        if(promoPick==1) {
            min = '';
            max = '';
            promoPick =0;
        }

        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
        
    });

    //BY CASA MATRIZ
    $('select[name="casamatriz"]').on("change", function(){
        console.log('casa_matriz');
        $('.no-more').addClass('hide');
        moneda = $('.moneda.active').data('moneda');
        marca = $('select[name="marca"]').val();
        categoria = $('select[name="categoria"]').val();
        promo360 = $('select[name="filtros_adicionales"]').val();
        casamatriz = $(this).val();
        max = $('#upper').val();
        min = $('#lower').val();
        search = $('#myInput').val();

        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    });

    //BY FILTROS ADICIONALES
    $('select[name="filtros_adicionales"]').on("change", function(){
        console.log('filtros_adicionales');
        $('.no-more').addClass('hide');
        moneda = $('.moneda.active').data('moneda');
        marca = $('select[name="marca"]').val();
        categoria = $('select[name="categoria"]').val();
        casamatriz = $('select[name="casamatriz"]').val();
        promo360 = $(this).val();
        max = $('#upper').val();
        min = $('#lower').val();
        search = $('#myInput').val();

        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);
    });

    //BY PAGE
    $('.more-vehicles').on("click", function(){
        console.log('get more vehicles');
        $('.no-more').addClass('hide');
        moneda = $('.moneda.active').data('moneda');
        marca = $('select[name="marca"]').val();
        categoria = $('select[name="categoria"]').val();
        promo360 = $('select[name="filtros_adicionales"]').val();
        casamatriz = $(this).val();
        max = $('#upper').val();
        min = $('#lower').val();

        page = parseInt($('input[name="currentpage"]').val()) + 1;
        $('input[name="currentpage"]').val(page);
        search = $('#myInput').val();

        getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, page, min, max, search);
    });



     /*
    *
    *    SEARCH VEHICULOS WITH AJAX
    *
    */



    function getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, page, min, max, search){

        $('.spiner-for-search ').removeClass('hide');
        $('.more-vehicles').removeClass('hide');

        $('input[name="currentpage"]').val(page);

        if(page == 1){
            $('.catalog__wrap-items .catalog__item').remove();
        }

        if(moneda == undefined){
            moneda = "";
        }
        console.log(moneda);

        $.ajax({
            url: '/autoexposite/wp-admin/admin-ajax.php?action=get_vehiculos_ajax',
            method: 'POST',
            data: { moneda: moneda, marca: marca, categoria: categoria, filtros_adicionales: promo360, casamatriz: casamatriz, page: page, min: min, max: max, search : search },
        }).done(function(response) {
            console.log(response);
            if(page === 1){
                $('.catalog__wrap-items').html("");
            }
            customHtml = '';
            $.each( response.vehiculos, function( key, value ) {
                
                moneda_print = '$';
                if(value.moneda == 'Quetzales'){
                    moneda_print = 'Q';
                }

                console.log(moneda_print)

                customHtml = customHtml + '<div class="catalog__item"><div class="catalog__item-header">';

                if(value.vista_virtual){
                    customHtml = customHtml + '<div class="catalog__vista360"><img src="https://expo.baccredomatic.com/wp-content/themes/autoexpo/images/icon-360.png" alt="Vista 360"></div>';
                }

                if(value.promo){
                    customHtml = customHtml + '<div class="catalog__item-header-promo">AUTOEXPO 2020</div>';
                }

                added = "";
                text = "A"
                if(value.added){
                    added = " my-garage";
                    text = "En"
                }

                titulo = value.titulo_corto;
                if(!value.titulo_corto) {
                    titulo = value.post_title;
                }


                customHtml = customHtml + '<a href="'+value.link+'"><img src="' + value.images['thumbnail'][0] + '" alt=""></a></div>';
                customHtml = customHtml + '<div class="catalog__item-body"><a href="'+value.link+'"><h3>';
                customHtml = customHtml + titulo+'</a></h3>';
                customHtml = customHtml +'<div class="price"><span>';
                customHtml = customHtml + value.marca +'</span>';
                customHtml = customHtml +'<span>';
                customHtml = customHtml +  moneda_print + " " + value.precio +'</span></div>';
                customHtml = customHtml + '<div class="buttons"><a href="javascript:void(0)" class="add-garage' +added+'"  data-id="'+value.ID+'"><span class="icon-timon"></span> '+text+' Favoritos</a>';
                customHtml = customHtml + '<a href="'+value.link+'">Ver detalle</a>';

                customHtml = customHtml + '</div></div></div>';

            });

            $('.catalog__wrap-items').append(customHtml);
            $('.spiner-for-search ').addClass('hide');

            if(response.vehiculos.length == 0){
                $('.no-more').removeClass('hide');
                $('.more-vehicles').addClass('hide');
            }
        }).fail(function() {
            $('.spiner-for-search ').addClass('hide');
        });

    }

    function getMarcasAjax(slug){

        $('.spiner-for-search ').removeClass('hide');

        $.ajax({
            url: '/autoexposite/wp-admin/admin-ajax.php?action=get_categoríes_by_marca',
            method: 'POST',
            data: { slug: slug },
        }).done(function(response) {

            customHtml = '';
            $('#categoria').empty();
            $('#categoria').append('<option disabled selected>Categoría</option>');
            $('#categoria').append('<option value="">Ver todos</option>');
            $.each( response.categorias, function( key, value ) {
                $('#categoria').append('<option value="'+value.slug+'">'+value.name+'</option>');
            });

            $('#filtros_adicionales').empty();
            $('#filtros_adicionales').append('<option disabled selected>Ver todos</option>');
            $('#filtros_adicionales').append('<option value="">Ver todos</option>');
            $.each( response.filtros_adicionales, function( key, value ) {
                $('#filtros_adicionales').append('<option value="'+value.slug+'">'+value.name+'</option>');
            });

            $('#casamatriz').empty();
            $('#casamatriz').append('<option disabled selected>Casa Matríz</option>');
            $('#casamatriz').append('<option value="">Ver todos</option>');
            $.each( response.casamatriz, function( key, value ) {
                $('#casamatriz').append('<option value="'+value.slug+'">'+value.name+'</option>');
            });

        }).fail(function() {

        });

    }









     /*
    *
    *    PRINT DATA
    *
    */


    $('.print-detalle').on("click", function(){
        printDiv('wrap-single');
    });

    // PRINT DATA
    function printDiv(divName){
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }













    /*
    *
    *       ADD TO GARAGE
    *
    */



   $(document).on("click", ".add-garage", function(){
        myGarage = getCookie('garage');
        id = $(this).data('id');
        $("nav #menu-item-21").addClass('items-add');
        $(".modal").removeClass('hide');
        $(".modal__content").center();


        $(this).addClass('my-garage');

        if(myGarage){
            garage = $.parseJSON(myGarage);
            if($.inArray(id, garage) == -1){
                garage.push(id);
                get_vehicle_ajax(id);
            }

        }else{
            garage = [];
            garage.push(id);
            get_vehicle_ajax(id);
        }
        if($('input[name="currentpage"]').val()){
            $(this).html('<span class="icon-timon"></span> En favoritos');
        }else{
            $(this).html('<span class=""></span> En favoritos');
        }



        val = JSON.stringify(garage);
        var now = new Date();
        now.setTime(+ now + (365 * 86400000)); //24 * 60 * 60 * 1000;
        document.cookie = "garage="+val+";expires="+now.toGMTString()+";path=/";

   });

   $(document).on("click", ".remove", function(){

        myGarage = getCookie('garage');

        id = $(this).data('id');

        garage = $.parseJSON(myGarage);
        if($.inArray(id, garage) >= 0){
            garage.splice($.inArray(id, garage),1);
            parent = $(this).parent().parent();
            parent.remove();
        }

        val = JSON.stringify(garage);
        var now = new Date();
        now.setTime(+ now + (365 * 86400000)); //24 * 60 * 60 * 1000;
        document.cookie = "garage="+val+";expires="+now.toGMTString()+";path=/";


        if(getCookie('garage').length > 0 && getCookie('garage') != "[]"){
            $("nav #menu-item-21").addClass('items-add');
        }else{
            $("nav #menu-item-21").removeClass('items-add');
            $('.my-cars .error-garage').removeClass('hide');
        }

        $('*[data-id="'+id+'"]').removeClass('my-garage');


        if($('input[name="currentpage"]').val()){
            $('*[data-id="'+id+'"]').html('<span class="icon-timon"></span> A favoritos');
        }else{
            $('*[data-id="'+id+'"]').html('<span class=""></span> A favoritos');
        }




    });

    // console.log(getCookie('garage'));
   if(getCookie('garage').length > 0 && getCookie('garage') != "[]"){
       $("nav #menu-item-21").addClass('items-add');
   }


   function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
  }


  function get_vehicle_ajax($id){
    $.ajax({
        url: '/autoexposite/wp-admin/admin-ajax.php?action=get_vehicle_ajax',
        method: 'POST',
        data: { id: id },
    }).done(function(response) {
        $('.my-cars .error-garage').addClass('hide');
        my_vehicle = response.vehiculo;

        if( my_vehicle.moneda == "Quetzales"){
            moneda = "Q";
        }else{
            moneda = "$";
        }

        titulo = my_vehicle.titulo_corto;
        if(!my_vehicle.titulo_corto) {
            titulo = my_vehicle.post_title;
        }

        customHtml = "";
        customHtml = customHtml + '<div class="my-cars__item"><div class="my-cars__item--header">'
        customHtml = customHtml + '<a href="'+ my_vehicle.link +'"><img src="'+my_vehicle.images["thumb-favs"][0]+'" alt="'+my_vehicle.post_title+'"></a></div>';
        customHtml = customHtml + '<div class="my-cars__item--body"><h3><a href="'+ my_vehicle.link +'">'+titulo+'</a></h3>';
        customHtml = customHtml + '<span>'+my_vehicle.marca+'</span>';
        customHtml = customHtml + '<span>'+moneda+my_vehicle.precio+'</span>';
        customHtml = customHtml + '</div><div class="my-cars__item--footer"><a href="javascript:void(0)" class="remove" data-id="'+my_vehicle.ID+'">Remover</a><a href="http://autoexpo-gt.bakingthecookie.com/?vehiculo='+my_vehicle.post_title+'#contacto"  class="red">Solicitar préstamo</a></div></div>';
        $('.my-cars__cars').append(customHtml);
    }).fail(function() {

    });

  }










  // filter price


  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  /* jshint ignore:start */
  
    var lowerSlider = document.querySelector('#lower');
    var  upperSlider = document.querySelector('#upper');

  
  if(lowerSlider && upperSlider){
            document.querySelector('#two').value=formatNumber(upperSlider.value);
            document.querySelector('#one').value=formatNumber(lowerSlider.value);

            var lowerVal = parseInt(lowerSlider.value);
            var upperVal = parseInt(upperSlider.value);

            upperSlider.oninput = function () {
                lowerVal = parseInt(lowerSlider.value);
                upperVal = parseInt(upperSlider.value);

                if (upperVal < lowerVal + 4) {
                    lowerSlider.value = upperVal - 4;
                    if (lowerVal == lowerSlider.min) {
                    upperSlider.value = 4;
                    }
                }
                document.querySelector('#two').value= formatNumber(this.value);
                $('input[name="product_page"]').val(1);
            };

            lowerSlider.oninput = function () {
                lowerVal = parseInt(lowerSlider.value);
                upperVal = parseInt(upperSlider.value);
                if (lowerVal > upperVal - 4) {
                    upperSlider.value = lowerVal + 4;
                    if (upperVal == upperSlider.max) {
                        lowerSlider.value = parseInt(upperSlider.max) - 4;
                    }
                }
                document.querySelector('#one').value= formatNumber(this.value);
                $('input[name="product_page"]').val(1);
            };
            /* jshint ignore:end */


            // PRODUCT SELECT RANGE
            $(upperSlider).mouseup(function() {
                    $('.no-more').addClass('hide');
                    moneda = $('.moneda.active').data('moneda');
                    marca = $('select[name="marca"]').val();
                    categoria = $('select[name="categoria"]').val();
                    promo360 = $('select[name="filtros_adicionales"]').val();
                    casamatriz = $('select[name="casamatriz"]').val();
                    max = $('#upper').val();
                    min = $('#lower').val();
                    search = $('#myInput').val();
                    getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);

            });

            // PRODUCT SELECT RANGE
            $(lowerSlider).mouseup(function() {
                    $('.no-more').addClass('hide');
                    moneda = $('.moneda.active').data('moneda');
                    marca = $('select[name="marca"]').val();
                    categoria = $('select[name="categoria"]').val();
                    promo360 = $('select[name="filtros_adicionales"]').val();
                    casamatriz = $('select[name="casamatriz"]').val();
                    max = $('#upper').val();
                    min = $('#lower').val();
                    search = $('#myInput').val();
                    getVehiculosAjax(moneda, marca, categoria, promo360, casamatriz, 1, min, max, search);

            });

    }





    // center modal
    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
        $(window).scrollTop()) + "px");
        // this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
        //                                             $(window).scrollLeft()) + "px");
        return this;
    }






});












