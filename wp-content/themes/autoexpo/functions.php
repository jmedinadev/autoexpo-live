<?php
/**
 * autoexpo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package autoexpo
 */

if ( ! function_exists( 'autoexpo_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function autoexpo_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on autoexpo, use a find and replace
		 * to change 'autoexpo' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'autoexpo', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'autoexpo' ),
			'menu-2' => esc_html__( 'Mobile', 'autoexpo' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'autoexpo_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'autoexpo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function autoexpo_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'autoexpo_content_width', 640 );
}
add_action( 'after_setup_theme', 'autoexpo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function autoexpo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'autoexpo' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'autoexpo' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'autoexpo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function autoexpo_scripts() {
	wp_enqueue_style( 'autoexpo-style', get_stylesheet_uri() );

	wp_enqueue_script( 'autoexpo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'autoexpo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'autoexpo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Crops

add_image_size( 'thumb-catalog', 300, 190, true);
add_image_size( 'thumb-favs', 250, 160, true);


function get_cars_taxonomy($term_id) {
	// var_dump($term_id);
	$args = array(
        'post_type' => array('vehiculos'),
        'posts_per_page' => '12',
        'post_status' => 'publish',
        'order' => 'DESC',
		'tax_query' => array(
            array(
                'taxonomy' => 'marca',
                'field' => 'slug',
                'terms' => $term_id
            )
        )
    );


    $posts = new WP_Query($args);
    foreach ($posts->posts as $key => $post) {

        $images = array();
        $thumb_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-catalog' );
        $images['thumbnail'] = $thumb_post;
        $post->images = $images;

        $post->precio = get_field('precio_con_iva', $post->ID);
        $post->promo = get_field('promocion_especial', $post->ID);
        $brands = get_terms( array(
		    'taxonomy' => 'casa_matriz',
		    'parent'   => 0
		) );
		foreach($brands as $brand):
			$post->marca = $brand->name;
			$post->brand_link = get_term_link($brand);
		endforeach;

		$moneys = get_terms( array(
		    'taxonomy' => 'moneda',
		    'parent'   => 0
		) );
		foreach($moneys as $money):
			$post->moneda = $money->name;
		endforeach;


        $post->link = get_permalink($post->ID);
    }
    return $posts->posts;
}



// REMOVE ADMIN BAR
add_filter( 'show_admin_bar', '__return_false' );




// Views


function gt_get_post_view() {
    $count = get_post_meta( get_the_ID(), 'post_views_count', true );
    return "$count views";
}

function gt_set_post_view() {
    $key = 'post_views_count';
    $post_id = get_the_ID();
    $count = (int) get_post_meta( $post_id, $key, true );
    $count++;
    update_post_meta( $post_id, $key, $count );
}

function gt_posts_column_views( $columns ) {
    $columns['post_views'] = 'Views';
    return $columns;
}

function gt_posts_custom_column_views( $column ) {
    if ( $column === 'post_views') {
        echo gt_get_post_view();
    }
}

add_filter( 'manage_posts_columns', 'gt_posts_column_views' );
add_action( 'manage_posts_custom_column', 'gt_posts_custom_column_views' );


function get_more_views() {
	$args = array(
        'post_type' => array('vehiculos'),
        'paged' => 1,
        'posts_per_page' => 5,
        'post_status' => 'publish',
        'order' => 'DESC',
        'orderby' => 'post_views_count',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key'  => 'post_views_count',
                'value' => 0,
                'compare' => '>'
            ),
        )
    );
    $post = new WP_Query($args);
    return $post->posts;
}

// culpable: Jonay Medina
// filtrar por autor los vehiculos
function posts_for_current_author($query) {
        global $pagenow;
  
    if( 'edit.php' != $pagenow || !$query->is_admin )
        return $query;
  
    if( is_user_logged_in() && is_author(get_current_user_id()) ) {
       global $user_ID;
       $query->set('author', $user_ID );
    }
    return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

//  el author al agregar un vehiculo nuevo se guarde automatico (taxonimia) Casa Matriz
function update_my_taxonomies($post_id, $post, $update){
	$current_user = wp_get_current_user();
	$tax = get_field( 'casa_matriz', 'user_'.get_current_user_id());
	$term = get_terms($tax, 'marca_casamatriz');
	// var_dump($term);
	if (in_array( 'author', $current_user->roles, true )) {
		if ($update) {
			if(current_user_can( 'edit_posts', $post_id ) && ($post->post_author == $current_user->ID)) {
			    if(! has_term( '', 'casa_matriz', $post_id )){
			        wp_set_object_terms( $post_id, $tax, 'casa_matriz' );
			    }
		     }
		}
		if ('post' == $post->post_type) {
			if(current_user_can( 'edit_posts', $post_id)) {
			    if(! has_term( '', 'casa_matriz', $post_id )){
			        wp_set_object_terms( $post_id, $tax, 'casa_matriz' );
			    }
		     }
		}
	}	
}

add_action('save_post','update_my_taxonomies', 10,3);


function add_admin_scripts( $hook ) {

    global $post;
    $current_user = wp_get_current_user();
	if (in_array( 'author', $current_user->roles, true )) {
	    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
	        if ( 'vehiculos' === $post->post_type ) {     
	            $term_id = get_field( 'casa_matriz', 'user_'.get_current_user_id());
			   	$marcas = get_field( 'marca_casamatriz', 'casa_matriz_'.$term_id);
			   	$listing = [];

			   	foreach ($marcas as $m) {
			   		$l = get_term( $m, 'marca' );
			   		array_push($listing, $l);
			   	}
			   	$listing = json_encode($listing);
	            echo "<script>

		              window.marcas_author = JSON.parse('{$listing}')
		            
	              	</script>";
	        }
	    }
	}
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 999 );


function load_marcas_author($hook) {
 	
 	$current_user = wp_get_current_user();
 	if (in_array( 'author', $current_user->roles, true )) {
    
    	wp_enqueue_script( 'filter-author-marcas', get_template_directory_uri() . '/js/filter-author-marcas.js', array(), '1.0.0', true );
 	}
}
add_action('admin_enqueue_scripts', 'load_marcas_author');


// Cuando es author remover el Recuadro de Casa matriz al editar o agregar un vehiculo
function remove_cuttax_metaboxes() {
	$current_user = wp_get_current_user();

	if (in_array( 'author', $current_user->roles, true )) {
	   $post_type = 'vehiculos';
	   $taxonomy = 'casa_matriz';
	   remove_meta_box( 'tagsdiv-'.$taxonomy, $post_type, 'side' );
	   remove_meta_box( 'casa_matrizdiv', $post_type, 'side' );
	}
}
add_action( 'add_meta_boxes_vehiculos' , 'remove_cuttax_metaboxes', 100 );


function wpcodex_add_author_support_for_posts() {
    add_post_type_support( 'post', 'author' );
}
add_action( 'init', 'wpcodex_add_author_support_for_posts' );


if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5fd1271808732',
		'title' => 'Casa Matriz',
		'fields' => array(
			array(
				'key' => 'field_5fd127281b91c',
				'label' => 'Casa matriz',
				'name' => 'casa_matriz',
				'type' => 'taxonomy',
				'instructions' => 'Editor pertenece a casa matriz',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'casa_matriz',
				'field_type' => 'select',
				'allow_null' => 0,
				'add_term' => 1,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'user_role',
					'operator' => '==',
					'value' => 'author',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_6005a91182d7a',
	'title' => 'Marca Casamatriz',
	'fields' => array(
		array(
			'key' => 'field_6005a93374f1e',
			'label' => 'Marca Casamatriz',
			'name' => 'marca_casamatriz',
			'type' => 'taxonomy',
			'instructions' => 'Cada casa matriz tiene varias mascas',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'marca',
			'field_type' => 'multi_select',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 1,
			'return_format' => 'id',
			'multiple' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'casa_matriz',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;


// Cuando es role Author remover permisos de publicar y se guarda en revision
function remove_author_publish_posts(){

    // $wp_roles is an instance of WP_Roles.
    global $wp_roles;
    $wp_roles->remove_cap( 'author', 'publish_posts' );
}

add_action( 'init', 'remove_author_publish_posts' );



if (!function_exists('debug')) {

   function debug($log) {
       if (true === WP_DEBUG) {
           if (is_array($log) || is_object($log)) {
               error_log(print_r($log, true));
           } else {
               error_log($log);
           }
       }
   }

}