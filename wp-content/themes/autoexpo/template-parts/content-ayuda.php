<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

?>

<div id="contacto" class="contacto">
	<div class="contacto__wrap wrap940">
		<div class="contacto__left">
			<?php echo get_the_content(); ?>
		</div>
	</div>
</div>
