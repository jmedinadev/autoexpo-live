<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('page-info'); ?>>
	<div class="entry-content">
		<h1><?php the_title(); ?></h1>
		<?php
		the_content();
		?>
		<!-- <hr style="margin-top: 65px;">
		<small>**Pregunta por nuestras promociones especiales al solicitar tu préstamo por Auto Expo Virtual**</small> -->
	</div><!-- .entry-content -->
	<?php autoexpo_post_thumbnail(); ?>
</article><!-- #post-<?php the_ID(); ?> -->
