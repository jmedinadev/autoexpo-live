<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

?>

<div class="promo-wrap">
	<h1>Promociones</h1>
	<?php
		$contenido_promociones = get_field('contenido_promociones');
		$i = 1;
		foreach($contenido_promociones as $key => $item):
	?>
	<div class="promo-wrap--div">
		<div class="promo-wrap__item <?php if($i>1): ?> promo-wrap__item--last <?php endif; ?>">
			<div class="promo-wrap__image">
				<img src="<?php echo $item['imagen']; ?>" alt="<?php echo $item['titulo']; ?>">
			</div>
			<div class="promo-wrap__desc">
				<h2><?php echo $item['titulo']; ?></h2>
				<?php echo $item['descripcion']; ?>
			</div>
			<a class="promo-wrap__item--link">Ver <span class="mas">más <img src="<?php bloginfo( 'template_url' ); ?>/images/flecha-abajo.svg" width="10" height="5" alt=""></span><span class="menos">menos <img src="<?php bloginfo( 'template_url' ); ?>/images/flecha-arriba.svg" width="10" height="5" alt=""></span></a>
		</div>
		<?php
			$descriptions = $item['promociones'];
			$num = count($descriptions);
		?>
		<div id="description<?php echo $i ?>" class="promo-wrap__item-container <?php if($num > 1): ?> wrap-group <?php endif; ?>">
			<?php
				foreach($descriptions as $key => $value):
			?>
			<div class="promo-wrap__item--descriptions <?php if($num > 1): ?> descriptions-group <?php endif; ?>">
				<?php echo $value['descripcion_de_promocion'] ?>
			</div>
			<?php endforeach; ?>
		</div>
		<?php $i++; ?>
	</div>
	<?php endforeach; ?>
</div>
