<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

$promo = get_field('promocion_especial');
$meses_de_financiamiento = get_field('meses_de_financiamiento');
$porcentaje_de_enganche = get_field('porcentaje_de_enganche');
$restricciones = get_field('restricciones');
$precio_con_iva = get_field('precio_con_iva');
$cuotas_desde = get_field('cuotas_desde');
$galeria = get_field('galeria_de_vehiculo');


$my_vehicles = str_replace('[',"",$_COOKIE["garage"]);
$my_vehicles = str_replace(']',"",$my_vehicles);
$my_vehicles = explode(',',$my_vehicles);

$added = false;

foreach ($my_vehicles as $key => $value) {
	if($value == $post->ID){
		$added = true;
	}
}

$moto = '';
$categories = get_the_terms($post->ID, 'categoria');
foreach ($categories as $key=>$value) {
	if($value->slug == 'moto') {
		$moto = $value->slug;
	}
}
// var_dump($moto);


$my_vehicles = str_replace('[',"",$_COOKIE["garage"]);
$my_vehicles = str_replace(']',"",$my_vehicles);
$my_vehicles = explode(',',$my_vehicles);
$added = false;

if(reset($my_vehicles) != "" && count($my_vehicles) > 0 ){
	foreach ($my_vehicles as $key => $value) {
		if($value == $post->ID){
			$added = true;
		}
	}
}

$vista_3d_interior = get_field('vista_3d_interior');
$vista_3d_exterior = get_field('vista_3d_exterior');


// Contador

$visitas = gt_set_post_view();

?>
<!-- Banner -->

<div class="wrap-single wrap940" id="wrap-single">
	<?php if(empty($vista_3d_interior)): ?>
	<div class="wrap-single__image">
		<div class="wrap-single__image-bac">
			<img width="84" height="29" src="https://expo.baccredomatic.com/wp-content/uploads/2019/06/Bac_credomatic_logo.png" class="custom-logo" alt="Auto Expo">
		</div>

		<?php if(empty($galeria)) : ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); ?>
			<img src="<?php echo $image['0'] ?>" alt="<?php the_title(); ?>">
		<?php else: ?>
		    <ul class="slide-cars">
		        <?php foreach( $galeria as $image ): ?>
		            <li>
	                     <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		            </li>
		        <?php endforeach; ?>
		    </ul>
		<?php endif; ?>
	</div>
	<?php else: ?>
	<div class="wrap-single__image">
		<div class="tabs">
			<div role="tabpanel" aria-labelledby="tab-1">
				<?php echo $vista_3d_exterior; ?>
			</div>
			<div role="tabpanel" aria-labelledby="tab-2" hidden>
				<?php echo $vista_3d_interior; ?>
			</div>
			<div role="tablist" aria-label="tabs">
				<button class="btn-tabs" role="tab" aria-selected="true" id="tab-1">Vista exterior</button>
				<button class="btn-tabs" role="tab" aria-selected="false" id="tab-2">Vista interior</button>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="wrap-single__content">
		<div class="texts">
			<div class="back no-print"><a href="<?php echo get_home_url(); ?>#catalogo"><span class="icon-back"></span>Regresar</a></div>
			<?php
				$years = get_the_terms(get_the_ID(), 'ano_vehiculo');
				$year = false;
				foreach($years as $year):
					$year = $year->name;
				endforeach;
			?>
			<?php
				$terms = get_the_terms( get_the_ID(), 'marca' );
				foreach($terms as $brand):
				$brand_image = get_field('logotipo_de_la_marca', 'term_'.$brand->term_id);

				$brand_link = get_term_link($brand);
			?>

				<h1><img src="<?php echo $brand_image['sizes']['thumbnail']; ?>" alt="<?php echo $brand_image['alt']; ?>" width="<?php echo $brand_image['width']; ?>" height="<?php echo $brand_image['height']; ?>"><?php the_title();?></h1>
			<?php endforeach; ?>
			<ul style="margin: 0;">
				<li>Modelo <?php echo $year; ?></li>
			</ul>
			<?php the_content(); ?>
			<?php if(!empty($promo)): ?>
			<div class="promo">
				<div class="tag">Promoción Especial</div>
				<ul>
					<li><?php echo $promo; ?></li>
				</ul>
			</div>
			<?php endif; ?>
			<hr>
			<span class="restriction">* Cuotas hasta <?php echo $meses_de_financiamiento; ?> meses, desde el <?php echo $porcentaje_de_enganche; ?>% de enganche.</span>
			<?php if(empty($moto)): ?>
			<br>
			<span class="restriction">* Aplica tasa preferencial para el primer año del crédito</span><br>
			<?php endif; ?>
			<span class="restriction"><?php echo $restricciones; ?>.</span><br>
			<span class="restriction">* Endoso de seguro con la aseguradora de tu preferencia.</span>
			<div class="options no-print">
				<a href="/calculo-de-credito/?vehicle=<?php echo $post->ID ?>" class="cta-plain cta--small">Calcular cuota</a>
				<a href="javascript:void(0)" class="cta cta--small print-detalle" >Imprimir detalle</a>
			</div>
		</div>
		<div class="side">
			<div class="side__top">
				<?php
					$moneys = get_the_terms(  get_the_ID(), 'moneda');
					$divisa = false;
					foreach($moneys as $money):
						$divisa = $money->name;
					endforeach;
				?>
				<div class="side__top-group mbottom10">
					<span style="padding-bottom:0;">Cuotas desde</span>
					<h3 style="font-size: 30px;"><?php if($divisa == 'Quetzales'): ?>Q <?php else :?>$ <?php endif; ?> <?php echo number_format($cuotas_desde, 2, '.', ','); ?> </h3>
					<?php if(empty($moto)): ?>
					<small>*Aplica para el primer año del<br>crédito con tasa preferencial</small>
					<br>
					<?php endif; ?>
					<small>*<?php echo $restricciones; ?></small>
				</div>
				<div class="side__top-group mbottom20">
					<span>Precios desde</span>

					<h3><?php if($divisa == 'Quetzales'): ?>Q <?php else :?>$ <?php endif; ?> <?php echo  number_format($precio_con_iva, 2, '.', ','); ?></h3>
				</div>
				<div class="site__top-buttons no-print">
					<a href="javascript:void(0)" class="cta cta--small cta--full mbottom10 add-garage" data-id="<?php echo $post->ID ?>" style="color: #fff !important"> <?php if($added){ echo "En";}else{ echo "A";} ?> favoritos</a>
					<a href="/contacto/" class="cta-white cta--small cta--full">Solicita tu préstamo</a>
				</div>
			</div>
			<img class="no-print" src="https://expo.baccredomatic.com/wp-content/uploads/2019/06/auto-expo-v2.gif" alt="Auto Expo">
		</div>
	</div>
</div>


<script>
	const tabs = document.querySelector('.tabs');
	const tabButtons = tabs.querySelectorAll('[role="tab"]');
	const tabPanels = tabs.querySelectorAll('[role="tabpanel"]');

	function handleTabClick(event) {
	  tabPanels.forEach(function(panel) {
	    panel.hidden = true;
	  });

	  tabButtons.forEach(function(tab) {
	    tab.setAttribute('aria-selected', false);
	  });

	  event.currentTarget.setAttribute('aria-selected', true);

	  const {id} = event.currentTarget;

	  const tabPanel = tabs.querySelector(`[aria-labelledby="${id}"]`);
	  tabPanel.hidden = false;
	}

	tabButtons.forEach(button => button.addEventListener('click', handleTabClick));
</script>
