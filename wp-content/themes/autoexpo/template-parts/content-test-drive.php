<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

?>

<div id="contacto" class="contacto">
	<h2 class="title-section">Agenda tu Test Drive</h2>
	<div class="contacto__wrap wrap940">
		<div class="contacto__left">
			<p>Por favor completa el siguiente formulario con tus datos para ponernos en contacto contigo lo antes posible.</p>
			<div id="opa-container"></div>
		</div>
	</div>
</div>
