<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

$vehiculos = get_vehiculos();
$more_views = get_more_views();
$marcas = get_all_taxonomies('marca');
$filtros_adicionales = get_all_taxonomies('filtros_adicionales');
$categorias = get_all_taxonomies('categoria');
$casa_matriz = get_all_taxonomies('casa_matriz');
$max_dols = get_max_price('$');
$min_dols = get_min_price('$');
$max = get_max_price();
$min = get_min_price();
$list_vehiculos = get_titles_vehiculos();
?>
<style>
/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}
/* 
input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}

input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
}

input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
  cursor: pointer;
} 
*/

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: #e9e9e9 !important; 
  /* color: #ffffff;  */
}

.autocomplete button {
    position: absolute;
    right: 0;
    top: 20px;
    background: none;
    border: none;
    pointer-events: none;
}

/* The Modal (background) */
.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 12; /* Sit on top */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0,0,0); /* Fallback color */
	background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
	/* background-color: #fefefe; */
	background-color: transparent;
	margin: 15% auto; /* 15% from the top and centered */
	padding: 20px;
	/* border: 1px solid #888; */
	width: 80%; /* Could be more or less, depending on screen size */
	text-align: center;
}

/* The Close Button */
.close {
	color: white;
	float: right;
	font-size: 18px;
	font-weight: bold;
}

.close:hover,
.close:focus {
	color: white;
	text-decoration: none;
	cursor: pointer;
}
</style>

<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span id="close" class="close">&times; Cerrar</span>
	<video id="bone" width="100%"  controls>
		<source src="<?php echo bloginfo('template_url'); ?>/images/AutoExpo-2020.mp4" type="video/mp4">
		Your browser does not support the video tag.
	</video>
  </div>

</div>


<div class="banner" style="background-image: url(<?php echo bloginfo('template_url'); ?>/images/hero-image-autoexpo-2021-carro-lujo.png);">
	<div class="banner__overlay"></div>
	<div class="banner__content">
		<h1 style="text-align: left;">AutoExpo Virtual</h1>
		<p style="text-align: left;">Powered by <img src="<?php echo bloginfo('template_url'); ?>/images/logo-bac-white.png" alt="Bac" width="59" height="24"></p>
	</div>
</div>
<!-- Banner -->
<input type="hidden" name="currentpage" value="1">
<div class="catalog">
	<div class="catalog__left">
		<!-- <h2 class="title-section-black mtop0 ptop30 mbottom50">AutoExpo 2020</h2>
		<div class="catalog__video">
			<video autoplay muted loop id="video">
			  <source src="<?php //echo bloginfo('template_url'); ?>/images/AutoExpo-2020.mp4" type="video/mp4">
			  Your browser does not support HTML5 video.
			</video>
			<div class="catalog__video--content">
				<p>
					El evento que reúne más de 30 marcas de autos y 11 marcas de motos regresa con esta innovadora edición 100% digital.
					<br><br>
					Del 19 al 25 de octubre, aprovecha nuestros precios especiales y promociones exclusivas, con la mejor cuota del mercado.
				</p>
				<a href="/auto-expo-2020" style="padding: 5px 10px; border: 3px solid #E4002B; color: white; background-color: #E4002B; font-size: 15px;">INGRESA AL EVENTO</a>
				<a href="" id="myBtn2" style="padding: 5px 10px; border: 3px solid white; background-color: transparent; font-size: 15px;">VER VIDEO</a>
			</div>
		</div> -->
		<div id="marcas" class="catalog__partners wrap940 mPS2id-target">
			<h2 class="title-section-black mtop0 mbottom50">Elige una marca para comenzar a buscar</h2>
			<div class="catalog__wrap-partners">
				<ul class="slide-partners">
				<?php
					$terms = get_terms( array(
					    'taxonomy' => 'marca',
					    'parent'   => 0
					) );
					foreach($terms as $brand):
					$brand_image = get_field('logotipo_de_la_marca', 'term_'.$brand->term_id);

					$brand_link = get_term_link($brand);
				?>
					<li>
						<div class="partner__item">
							<a href="<?php echo $brand_link; ?>"></a>
							<img src="<?php echo $brand_image['sizes']['thumbnail']; ?>" alt="<?php echo $brand_image['alt']; ?>" width="<?php echo $brand_image['width']; ?>" height="<?php echo $brand_image['height']; ?>">
						</div>
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<div id="catalogo" class="catalog__header mPS2id-target">
			<div class="catalog__wrap wrap940">
				<form>
					<div class="opciones">
						<h2>Catálogo</h2>
						<div class="precios">
							<div class="radios">
								<span>Rango de precios</span>
								<label for="dolar">
									<input id="dolar" type="radio" name="moneda" checked>
									<span class="moneda" data-moneda="$">$</span>
								</label>
								<label for="quetzal">
									<input id="quetzal" type="radio" name="moneda">
									<span class="moneda" data-moneda="Q">Q</span>
								</label>
							</div>
							<div class="slider">
								<fieldset class="filter-price">
									<div class="filter-price-content">

										<div class="price-wrap">
											<div class="price-wrap-1">
												<label for="one"></label>
												<input id="one" readonly>
											</div>
										</div>


										<div class="price-field">
											<input type="range" class="range hide" min="<?php echo $min_dols; ?>" max="<?php echo $max_dols; ?>" id="lower_dols" value="<?php echo $min_dols; ?>">
											<input type="range" class="range hide" min="<?php echo $min_dols; ?>" max="<?php echo $max_dols; ?>" id="upper_dols" value="<?php echo $max_dols; ?>">

											<input type="range" class="range" min="<?php echo $min; ?>" max="<?php echo $max; ?>" id="lower" value="<?php echo $min; ?>">
											<input type="range" class="range" min="<?php echo $min; ?>" max="<?php echo $max; ?>" id="upper" value="<?php echo $max; ?>">
										</div>


										<div class="price-wrap">
											<div class="price-wrap-2">
												<label for="two"></label>
												<input id="two" readonly>
											</div>
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
					<!-- <div class="content-search"> -->
						<div class="autocomplete" style="width:100%;">
							<input id="myInput" type="text" name="search" placeholder="Busca el modelo de tu interés" style="height: 47px; border: 0; border-bottom: 1px solid #979797;" />
							<button><img src="<?php bloginfo( 'template_url' ); ?>/images/lupa.svg" width="15" height="15"></button>
						</div>
						<!-- <input type="submit"> -->
					<!-- </div> -->
					<div class="dropdowns">
						<select name="marca" id="marca">
							<option disabled selected>Escoge una marca</option>
							<option value="">Ver todos</option>
							<?php foreach ($marcas as $key => $value) { ?>
								<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
							<?php } ?>
						</select>
						<select name="filtros_adicionales" id="filtros_adicionales">
							<option value="">Ver todos</option>
							<?php foreach ($filtros_adicionales as $key => $value) { ?>
								<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
							<?php } ?>
						</select>
						<select name="categoria" id="categoria">
							<option disabled selected>Categoría</option>
							<option value="">Ver todos</option>
							<?php foreach ($categorias as $key => $value) { ?>
								<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
							<?php } ?>
						</select>
						<select name="casamatriz" id="casamatriz">
							<option disabled selected>Casa Matríz</option>
							<option value="">Ver todos</option>
							<?php foreach ($casa_matriz as $key => $value) { ?>
								<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>
							<?php } ?>
						</select>
					</div>
				</form>
			</div>
		</div>
		<div class="catalog__mobile">
			<!-- <br><br> -->
			<h2 class="title-section-black">Los más vistos</h2>
			<!-- <a href="#" class="di-block pleft30 pright30">Ver más <img src="<?php bloginfo( 'template_url' ); ?>/images/flecha.svg" alt="Ver más" width="15" height="12"></a> -->
			<ul class="slide-mobile">
				<?php foreach($more_views as $key=>$value): ?>
				<li>
					<div class="view-block">
						<a href="<?php the_permalink( $value->ID ); ?>"></a>
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value->ID ), 'medium' ); ?>
						<img src="<?php echo $image['0'] ?>" alt="<?php the_title(); ?>">
						<h5><?php echo $value->post_title ?></h5>
						<span class="view-block__link">Ver detalle</span>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="catalog__content">
			<!-- <a class="filter-promo" href="">
				<img class="large" src="<?php echo bloginfo('template_url'); ?>/images/promos2020.png" alt="Autoexpo 2020">
				<img class="small" src="<?php echo bloginfo('template_url'); ?>/images/promos2020small.png" alt="Autoexpo 2020">
			</a> -->

			<a class="filter-pickup" href="">
				<img class="large" src="<?php echo bloginfo('template_url'); ?>/images/pickups2021.png" alt="Autoexpo 2020">
				<img class="small" src="<?php echo bloginfo('template_url'); ?>/images/pickups2021small.png" alt="Autoexpo 2020">
			</a>

			<div class="catalog__vehicles">
				<div class="catalog__wrap-items wrap940">
					<?php foreach($vehiculos as $item): ?>
					<div class="catalog__item">
						<?php if(!empty($item->vista_virtual)): ?>
							<div class="catalog__vista360">
								<img src="<?php bloginfo('template_url'); ?>/images/icon-360.png" alt="Vista 360">
							</div>
						<?php endif; ?>
						<div class="catalog__item-header">
							<?php if(!empty($item->promo)): ?>
							<div class="catalog__item-header-promo">AUTOEXPO 2020</div>
							<?php endif; ?>
							<a href="<?php echo $item->link; ?>">
								<img src="<?php echo $item->images['thumbnail'][0] ?>" alt="">
							</a>
						</div>
						<div class="catalog__item-body">
							<?php if(empty($item->titulo_corto)): ?>
								<h3><a href="<?php echo $item->link; ?>"><?php echo $item->post_title; ?></a></h3>
							<?php else: ?>
								<h3><a href="<?php echo $item->link; ?>"><?php echo $item->titulo_corto; ?></a></h3>
							<?php endif; ?>
							<div class="price">
								<span><?php echo $item->marca; ?></span>
								<span><?php if($item->moneda == 'Quetzales'): ?>Q<?php else:?>$<?php endif;?> <?php echo $item->precio ?></span>
							</div>
							<div class="buttons">
								<!-- Si el vehiculo se guarda en mi garage agregar esta clase al siguiente link 'my-garage' -->
								<a href="javascript:void(0)" class="add-garage <?php if($item->added){ echo "my-garage";} ?>" data-id="<?php echo $item->ID ?>"><span class="icon-timon"></span>  <?php if($item->added){ echo "En";}else{ echo "A";} ?> favoritos</a>
								<a href="<?php echo $item->link; ?>">Ver detalle</a>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<div class="text-center mtop80">
					<div class="spiner-for-search hide">
						<div class="cssload-container">
							<div class="cssload-zenith"></div>
						</div>
					</div>
					<h2 class="no-more hide">No hay más vehículos</h2>
					<a href="javascript:void(0)" class="cta more-vehicles">Ver más</a>
				</div>
			</div>
			<!-- Vehicles -->
		</div>
	</div>
	<div class="catalog__right">
		<!-- <div class="promo-block"><span>Promoción</span></div> -->
		<h2 class="title-section-black mtop0">Los más vistos</h2>
		<!-- <a href="#">Ver más  <img src="<?php bloginfo( 'template_url' ); ?>/images/flecha.svg" alt="Ver más" width="15" height="12"></a> -->
		<div class="view-block-wrapper">
			<?php foreach($more_views as $key=>$value): ?>
			<div class="view-block">
				<a href="<?php the_permalink( $value->ID ); ?>"></a>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value->ID ), 'medium' ); ?>
				<img src="<?php echo $image['0'] ?>" alt="<?php the_title(); ?>">
				<h5><?php echo $value->post_title ?></h5>
				<span class="view-block__link">Ver detalle</span>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<!-- Catalog -->

<script>
function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
			  inp.value = this.getElementsByTagName("input")[0].value;
			  $('#myInput').trigger("enterKey");
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var countries = <?php echo $list_vehiculos; ?>;

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);

</script>