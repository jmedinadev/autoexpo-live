<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

?>
<style>
    .ptop50{
        padding-top: 50px;
    }
    .ptop100{
        padding-top: 100px;
    }
    #auto-expo-2020 iframe {
        display: block;
        width: 100%;
        height: 720px;
        border: 0;
    }
    .page-template-page-auto-expo-2020 .header {
        display: none !important;
    }
    .page-template-page-auto-expo-2020 .site-content {
        padding-top: 0 !important;
    }.page-template-page-auto-expo-2020 button#responsive-menu-button {
        display: none;
    }
    @media screen and (min-width: 1024px) {
        #auto-expo-2020 iframe {
            height: 100vh;
        }
    }
</style>
<div id="auto-expo-2020" class="auto">
    <iframe src="https://storage.googleapis.com/gbv/bac/autoexpo/2020/index.html" width="100%" height="100%" allow="gyroscope; accelerometer" allowfullscreen=""></iframe>
</div>
