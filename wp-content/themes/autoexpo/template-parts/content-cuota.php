<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

$vehicleID = $_GET['vehicle'];
$enganche = $_GET['enganche'];
$plazos = $_GET['plazos'];
$vehicle = get_vehicle_by_id($vehicleID);


$moneda = "$";
if($vehicle->moneda == "Quetzales"){
	$moneda = "Q";
}

$cotizador = null;
if($plazos && $enganche != null){
	$cotizador = cotizador($moneda, $vehicle->precio_int, $enganche, $plazos);
}


?>
<div class="back wrap940 ptop30"><a href="<?php echo $vehicle->link ?>"><span class="icon-back"></span>Regresar al detalle</a>
</div>
<div class="contacto">
	<h2 class="title-section">Calcula tu cuota</h2>
	<div class="contacto__wrap wrap940">
		<div class="contacto__form">
			<form>
				<input type="hidden" name="vehicle" value="<?php echo $vehicleID ?>">
				<div class="form">
					<div class="form-large">
						<h3><?php echo $vehicle->post_title ?></h3>
					</div>
					<div class="form-small mbottom10">
						<label>
							Valor
							<input type="text" value="<?php echo $vehicle->precio ?>" readonly>
							<input type="hidden" name="valor" value="<?php echo $vehicle->precio_int ?>" readonly>
						</label>
					</div>
					<div class="form-small mbottom10">
						<label>
							Enganche
							<select name="enganche" id=""><!--
								<option value="0" <?php //if($enganche == "0"){ echo "selected"; } ?> >0%</option> -->
								<!-- <option value="0.15" <?php //if($enganche == "0.15"){ echo "selected"; } ?> >15%</option> -->
								<option value="0.20" <?php if($enganche == "0.20"){ echo "selected"; } ?> >20%</option>
								<option value="0.30" <?php if($enganche == "0.30"){ echo "selected"; } ?> >30%</option>
								<option value="0.40" <?php if($enganche == "0.40"){ echo "selected"; } ?> >40%</option>
								<option value="0.50" <?php if($enganche == "0.50"){ echo "selected"; } ?> >50%</option>
							</select>
						</label>
					</div>
					<div class="form-small">
						<label>
							Plazos
							<select name="plazos" id="">
								<option value="6" <?php if($plazos == "6"){ echo "selected"; } ?> >6</option>
								<option value="12" <?php if($plazos == "12"){ echo "selected"; } ?> >12</option>
								<option value="18" <?php if($plazos == "18"){ echo "selected"; } ?> >18</option>
								<option value="24" <?php if($plazos == "24"){ echo "selected"; } ?> >24</option>
								<option value="30" <?php if($plazos == "30"){ echo "selected"; } ?> >30</option>
								<option value="36" <?php if($plazos == "36"){ echo "selected"; } ?> >36</option>
								<option value="42" <?php if($plazos == "42"){ echo "selected"; } ?> >42</option>
								<option value="48" <?php if($plazos == "48"){ echo "selected"; } ?> >48</option>
								<option value="54" <?php if($plazos == "54"){ echo "selected"; } ?> >54</option>
								<option value="60" <?php if($plazos == "60"){ echo "selected"; } ?> >60</option>
								<option value="66" <?php if($plazos == "66"){ echo "selected"; } ?> >66</option>
								<option value="72" <?php if($plazos == "72"){ echo "selected"; } ?> >72</option>
								<option value="84" <?php if($plazos == "84"){ echo "selected"; } ?> >84</option>
							</select>
						</label>
					</div>
					<div class="form-small ptop30 text-right">
						<!-- <?php //if($cotizador){ ?>
							<span s>Interes: <?php //echo $cotizador["tasa"]*100 ?>%</span>
						<?php //} ?> -->
					</div>
					<div class="form-large ptop30 text-center">
						<button type="submit" class="cta cta--medium small-mbottom20">Calcula tu cuota</button>
						<?php if($cotizador) { ?>
							<a href="/contacto" class="cta-plain cta--medium">Solicita tu préstamo</a>
						<?php } ?>
					</div>
				</div>
			</form>
			<?php if($cotizador){ ?>
				<div class="resultado ptop30">
					<p class="mbottom10"><strong>Valor de enganche:</strong> <?php echo $moneda; echo $cotizador["valor_del_enganche"] ?></p>
					<p class="mbottom10"><strong>Valor a financiar:</strong> <?php echo $moneda; echo $cotizador["valor_a_financiar"] ?></p>
					<p class="mbottom10"><strong>Cuota mensual:</strong> <?php echo $moneda; echo $cotizador["cuoata_mensual"] ?></p>
					<p><small>* La cuota mensual no incluye seguros</small><br><small>* Aplica tasa preferencial para el primer año del crédito</small></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
