<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package autoexpo
 */

?>
<style>
    .temporizador {
        font-size: 60px;
        font-weight: 800;
    }
    .temporizador span {
        position: relative;
        margin: 0 10px
    }

    .temporizador span:after {
        position: absolute;
        bottom: -20px;
        font-size: 15px;
        left: 50%;
        transform: translateX(-50%);
        color: #E4002B;
    }
    .temporizador span.dias:after {
        content: "días";
    }
    .temporizador span.horas:after {
        content: "horas";
    }
    .temporizador span.minutos:after {
        content: "minutos";
    }
    .temporizador span.segundos:after {
        content: "segundos";
    }
    @media screen and (min-width: 1024px){
        .temporizador {
            font-size: 65px;
        }
    }
    .ptop50{
        padding-top: 50px;
    }
    .ptop100{
        padding-top: 100px;
    }
</style>
<div id="auto-expo-2020" class="auto">
	<!-- <h2 class="title-section">Auto Expo 2020</h2> -->
    <div class="text-center ptop50">
        <p>Las mejores marcas y promociones, con la mejor cuota del mercado, estarán disponibles para ti en:</p>
    </div>

    <!-- <h4 class="color--white">Muy pronto</h4> -->
    <div class="temporizador text-center">
        <span class="dias">0</span>:<span class="horas">00</span>:<span class="minutos">00</span>:<span class="segundos">00</span>
    </div>

    <div class="ptop50 text-center">
        <p>
        <strong>Fechas:</strong><br> 19 a 25 de octubre
        <br>
        <br>
        <strong>Horarios de atención:</strong>
        <br>
        Lunes a jueves de 8:00 a 20:00 horas <br>
        Viernes a domingo es de 8:00 a 21:00 horas
        </p>
    </div>
</div>

<script>
(function($){
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 seconds for example


    // TEMPORIZADOR
    function pad_with_zeroes(number, length) {
        var my_string = '' + number;
            while (my_string.length < length) {
            my_string = '0' + my_string;
        }
        return my_string;
    }

    var date_lanzamiento = new Date( "Oct 19, 2020 00:00:00" );

    function temporizador(){
        var date1, date2;
        date1 = new Date();
        date2 = date_lanzamiento;
        var diff = date1 - date2;
        var res = Math.abs(diff) / 1000;
        // console.log(res);

        // get total days between two dates
        var days = Math.floor(res / 86400);
        // document.write("<br>Difference (Days): "+days);
        $(".dias").html(days);

        // get hours
        var hours = Math.floor(res / 3600) % 24;
        hours = pad_with_zeroes(hours, 2);
        // document.write("<br>Difference (Hours): "+pad_with_zeroes(hours, 2));
        $(".horas").html(hours);

        // get minutes
        var minutes = Math.floor(res / 60) % 60;
        minutes = pad_with_zeroes(minutes, 2);
        // document.write("<br>Difference (Minutes): "+pad_with_zeroes(minutes, 2));
        $(".minutos").html(minutes);

        // get seconds
        var seconds = res % 60;
        seconds = pad_with_zeroes(Math.round(seconds), 2);
        // document.write("<br>Difference (Seconds): "+pad_with_zeroes(Math.round(seconds), 2));
        $(".segundos").html(seconds);

        setTimeout(function(){
            temporizador();
        }, 1000);
    }

    var date1, date2;
    date1 = new Date();
    date2 = date_lanzamiento;
    var diff = date1 - date2;
    if(diff < 0){
        temporizador();
        $(".content-temporizador").css("display", "block");
    }
})(jQuery);
</script>