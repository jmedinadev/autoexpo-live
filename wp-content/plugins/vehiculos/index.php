<?php
/**
 * Plugin Name: Modulo de Vehículos
 * Description: Administra la información para Vehículos
 * Version: 1.0
 * Author: Milkncookies <@mnc.gt>
 * Author URI: mailto:@mnc.gt
 * License
 */



// Vehiculos
function get_vehiculos ($sector = null) {

    // $vehiculos = search_vehiculos(1, 12, '$', 'hyundai', 'pick-up', 'canella');
    $vehiculos = search_vehiculos(1, 12, null, $sector, null, null);
    return $vehiculos;
}

// Vehiculos with call ajx
function get_vehiculos_ajax () {

    header('Content-Type: application/json');

    $response = array(
        'status' => 400,
        'message' => 'No se encontraron vehículos',
    );

    $page = $_POST['page'];
    $moneda = $_POST['moneda'] ? $_POST['moneda'] : null;
    $marca = $_POST['marca'] ? $_POST['marca'] : null;
    $categoria = $_POST['categoria'] ? $_POST['categoria'] : null;
    $casamatriz = $_POST['casamatriz'] ? $_POST['casamatriz'] : null;
    $promo360 = $_POST['filtros_adicionales'] ? $_POST['filtros_adicionales'] : null;
    $page = $_POST['page'] ? $_POST['page'] : 1;
    $min = $_POST['min'] ? $_POST['min'] : 0;
    $max = $_POST['max'] ? $_POST['max'] : 1000000;

    $search = $_POST['search'] ? $_POST['search'] : "";
    // var_dump($search);

    $response['vehiculos'] = search_vehiculos($page, 12, $moneda, $marca, $promo360, $categoria, $casamatriz, $min, $max, $search);
    $response['status'] = 200;
    $response['message'] = 'success';
    // var_dump($response);
    wp_send_json($response);
    echo json_encode($response);
    die();
}
add_action( 'wp_ajax_nopriv_get_vehiculos_ajax', 'get_vehiculos_ajax' );
add_action( 'wp_ajax_get_vehiculos_ajax', 'get_vehiculos_ajax' );



function search_vehiculos($page, $perpage, $moneda = null, $marca = null, $promo360 = null, $categoria = null, $casamatriz = null, $min = 0, $max = 1000000, $search = ""){
    
    $args = array(
        'post_type' => array('vehiculos'),
        'paged' => $page,
        'posts_per_page' => $perpage,
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'title',
    );

    if($search == ""){

        // SEARCH BY MONEDA

        $args['tax_query'] = array(
            'relation' => 'AND',
        );

        if(!$moneda){
            $monedaTax = array(
                'taxonomy' => 'moneda',
                'field' => 'slug',
                'terms'    => ['quetzales', 'dolares'],
                'operator' => 'IN',
            );

            $args['tax_query'][] = array(
                $monedaTax
            );
        }


        if($moneda == "Q"){
            $monedaTax = array(
                'taxonomy' => 'moneda',
                'field' => 'slug',
                'terms'    => 'quetzales',
            );

            $args['tax_query'][] = array(
                $monedaTax
            );
        }

        if($moneda == "$"){
            $monedaTax = array(
                'taxonomy' => 'moneda',
                'field' => 'slug',
                'terms'    => 'dolares',
            );

            $args['tax_query'][] = array(
                $monedaTax
            );
        }



        // SEARCH BY MARCA
        if($marca){
            $marcaTax = array(
                'taxonomy' => 'marca',
                'field' => 'slug',
                'terms'    => $marca,
            );
            $args['tax_query'][] = $marcaTax;
        }


        // SEARCH FILTROS ADICIONALES
        if($promo360){
            $promo360Tax = array(
                'taxonomy' => 'filtros_adicionales',
                'field' => 'slug',
                'terms'    => $promo360,
            );
            $args['tax_query'][] = $promo360Tax;
        }


        // SEARCH BY CATEGORIA
        if($categoria){
            $categoriaTax = array(
                'taxonomy' => 'categoria',
                'field' => 'slug',
                'terms'    => $categoria,
            );
            $args['tax_query'][] = $categoriaTax;
        }


        // SEARCH BY CASA MATRIZ
        if($casamatriz){
            $casamatrizTax = array(
                'taxonomy' => 'casa_matriz',
                'field' => 'slug',
                'terms'    => $casamatriz,
            );
            $args['tax_query'][] = $casamatrizTax;
        }


        $precio = array(
            'key' => 'precio_con_iva',
            'value' => array($min, $max),
            'compare' => 'BETWEEN',
            'type' => 'NUMERIC'
        );
        $args['meta_query'] = array(
            $precio
        );
    }else{
        $args["s"] = $search;
    }
    // var_dump($args);
    $posts = new WP_Query($args);
    foreach ($posts->posts as $key => $post) {

        $images = array();
        $thumb_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-catalog' );
        $images['thumbnail'] = $thumb_post;
        $post->images = $images;

        $post->precio = number_format(get_field('precio_con_iva', $post->ID), 2, '.', ',');
        $post->promo = get_field('promocion_especial', $post->ID);
        $post->titulo_corto = get_field('titulo_corto', $post->ID);
        $post->vista_virtual = get_field('vista_3d_interior', $post->ID);

        $brands = get_the_terms( $post->ID, 'marca');
        foreach($brands as $brand):
            $post->marca = $brand->name;
            $post->brand_link = get_term_link($brand);
        endforeach;

        $moneys = get_the_terms( $post->ID, 'moneda');

        foreach($moneys as $money):
            $post->moneda = $money->name;
        endforeach;

        $my_vehicles = [];
        if( isset($_COOKIE["garage"]) ) {
            $my_vehicles = str_replace('[',"",$_COOKIE["garage"]);
            $my_vehicles = str_replace(']',"",$my_vehicles);
            $my_vehicles = explode(',',$my_vehicles);
        }

        $post->added = false;

        if(reset($my_vehicles) != "" && count($my_vehicles) > 0 ){
            foreach ($my_vehicles as $key => $value) {
                if($value == $post->ID){
                    $post->added = true;
                }
            }
        }


        $post->link = get_permalink($post->ID);
    }
    return $posts->posts;
}


function get_all_taxonomies($find){
    $terms = get_terms( array(
        'taxonomy' => $find,
        'hide_empty' => false,
    ) );

    return $terms;
}



// GET VIHICLE BY ID
function get_vehicle_ajax () {

    header('Content-Type: application/json');

    $response = array(
        'status' => 400,
        'message' => 'No se encontraron vehículos',
    );

    $post_id = $_POST['id'];
    $post = get_vehicle_by_id($post_id);
    $response['vehiculo'] = $post;
    $response['status'] = 200;
    $response['message'] = 'success';

    echo json_encode($response);
    die();
}
add_action( 'wp_ajax_nopriv_get_vehicle_ajax', 'get_vehicle_ajax' );
add_action( 'wp_ajax_get_vehicle_ajax', 'get_vehicle_ajax' );


function get_vehicle_by_id($post_id){
    $post = get_post($post_id);

    $images = array();
    $thumb_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-catalog' );
    $thumb_fav = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-favs' );
    $images['thumbnail'] = $thumb_post;
    $images['thumb-favs'] = $thumb_fav;
    $post->images = $images;

    $post->precio = number_format(get_field('precio_con_iva', $post->ID), 2, '.', ',');
    $post->precio_int = get_field('precio_con_iva', $post->ID);
    $post->promo = get_field('promocion_especial', $post->ID);
    $post->titulo_corto = get_field('titulo_corto', $post->ID);
    $post->vista_virtual = get_field('vista_3d_interior', $post->ID);

    $brands = get_the_terms( $post->ID, 'marca');
    foreach($brands as $brand):
        $post->marca = $brand->name;
        $post->brand_link = get_term_link($brand);
    endforeach;

    $moneys = get_the_terms( $post->ID, 'moneda');

    foreach($moneys as $money):
        $post->moneda = $money->name;
    endforeach;


    $post->link = get_permalink($post->ID);

    return $post;
}


function get_max_price($moneda = 'Q'){

    $moneda = $moneda == "$" ? get_term_by('slug', 'dolares', 'moneda') : get_term_by('slug', 'quetzales', 'moneda');;
    global $wpdb;
    $query = "SELECT max(cast(meta_value as unsigned)) FROM $wpdb->postmeta WHERE meta_key='precio_con_iva' AND 
    post_id IN (
        SELECT object_id 
        FROM $wpdb->term_relationships AS TR 
            INNER JOIN $wpdb->term_taxonomy AS TT ON TR.term_taxonomy_id = TT.term_taxonomy_id
            INNER JOIN $wpdb->terms AS T ON TT.term_id = T.term_id
        WHERE TT.taxonomy = 'moneda' AND T.term_id = $moneda->term_id
    )";
    $the_max = $wpdb->get_var($query);
    return $the_max;
}


function get_min_price($moneda = "Q"){
    $moneda = $moneda == "$" ? get_term_by('slug', 'dolares', 'moneda') : get_term_by('slug', 'quetzales', 'moneda');;
    global $wpdb;
    $query = "SELECT min(cast(meta_value as unsigned)) FROM $wpdb->postmeta WHERE meta_key='precio_con_iva' AND 
    post_id IN (
        SELECT object_id 
        FROM $wpdb->term_relationships AS TR 
            INNER JOIN $wpdb->term_taxonomy AS TT ON TR.term_taxonomy_id = TT.term_taxonomy_id
            INNER JOIN $wpdb->terms AS T ON TT.term_id = T.term_id
        WHERE TT.taxonomy = 'moneda' AND T.term_id = $moneda->term_id
    )";
    $the_max = $wpdb->get_var($query);
    return $the_max;
}



function get_categoríes_by_marca(){

    header('Content-Type: application/json');

    $slug = $_POST['slug'];

    $response = obtener_categorias_casas($slug);

    echo json_encode($response);
    die();

}
add_action( 'wp_ajax_nopriv_get_categoríes_by_marca', 'get_categoríes_by_marca' );
add_action( 'wp_ajax_get_categoríes_by_marca', 'get_categoríes_by_marca' );




function obtener_categorias_casas($slug){
    $args = array(
        'post_type' => array('vehiculos'),
        'paged' => 1,
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'title'
    );

    if($slug){
        $marcaTax = array(
            'taxonomy' => 'marca',
            'field' => 'slug',
            'terms'    => $slug,
        );
        $args['tax_query'][] = $marcaTax;
    }

    $posts = new WP_Query($args);

    $categories = array();
    $matriz = array();
    $promo360 = array();

    foreach ($posts->posts as $key => $post) {
        $categorias = get_the_terms( $post->ID, 'categoria');
        foreach($categorias as $categoria) {
            $categories[] = $categoria->slug;
        }

        // $adicionales = get_the_terms( $post->ID, 'filtros_adicionales');
        // foreach($adicionales as $adicional) {
        //     $promo360[] = $adicional->slug;
        // }

        $casas = get_the_terms( $post->ID, 'casa_matriz');
        if($casas) {
            $matriz[] = $casas[0]->slug;
        }
    }

    $result = array_unique($categories);
    $result_casa = array_unique($matriz);
    // $result_adicionales = array_unique($promo360);

    $response = array(
        'categorias' => array(),
        'casamatriz' => array(),
        // 'filtros_adicionales' => array(),
    );
    foreach ($result as $new_category) {
       $response['categorias'][] = get_term_by('slug', $new_category, 'categoria');
    }

    foreach ($result_casa as $new_casa) {
        $response['casamatriz'][] = get_term_by('slug', $new_casa, 'casa_matriz');
    }

    // foreach ($result_adicionales as $new_adicional) {
    //     $response['filtros_adicionales'][] = get_term_by('slug', $new_adicional, 'filtros_adicionales');
    // }

    return $response;
}



function get_titles_vehiculos(){
        $page = 1;

    $args = array(
        'post_type' => array('vehiculos'),
        'paged' => $page,
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'title'
    );

    $posts = new WP_Query($args);
    // var_dump(count($posts->posts));
    $vehiculos = array();
    foreach ($posts->posts as $key => $post) {
       $vehiculos[] = mb_convert_encoding($post->post_title, 'UTF-8', 'UTF-8');
    }
    // $vehiculos = mb_convert_encoding($vehiculos, 'UTF-8', 'UTF-8');
    // var_dump(count($vehiculos));
    // var_dump($vehiculos);
    return json_encode($vehiculos);
    // wp_send_json($vehiculos);
}


// // Vehiculos with call ajx
// function get_vehiculos_search_ajax () {

//     header('Content-Type: application/json');

//     $response = array(
//         'status' => 400,
//         'message' => 'No se encontraron vehículos',
//     );

//     $page = $_POST['page'];
//     $search = $_POST['search'];
    
//     $response['vehiculos'] = get_search_vehiculos($page, 12, $search);
//     $response['status'] = 200;
//     $response['message'] = 'success';
//     // var_dump($response);
//     wp_send_json($response);
// }
// add_action( 'wp_ajax_nopriv_get_vehiculos_search_ajax', 'get_vehiculos_search_ajax' );
// add_action( 'wp_ajax_get_vehiculos_search_ajax', 'get_vehiculos_search_ajax' );



// function get_search_vehiculos($page, $perpage, $search){


//     $args = array(
//         'post_type' => array('vehiculos'),
//         'paged' => $page,
//         'posts_per_page' => $perpage,
//         'post_status' => 'publish',
//         'order' => 'ASC',
//         'orderby' => 'title',
//         's' => $search
//     );
    

//     $posts = new WP_Query($args);
//     foreach ($posts->posts as $key => $post) {

//         $images = array();
//         $thumb_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-catalog' );
//         $images['thumbnail'] = $thumb_post;
//         $post->images = $images;

//         $post->precio = number_format(get_field('precio_con_iva', $post->ID), 2, '.', ',');
//         $post->promo = get_field('promocion_especial', $post->ID);
//         $post->titulo_corto = get_field('titulo_corto', $post->ID);
//         $post->vista_virtual = get_field('vista_3d_interior', $post->ID);

//         $brands = get_the_terms( $post->ID, 'marca');
//         foreach($brands as $brand):
//             $post->marca = $brand->name;
//             $post->brand_link = get_term_link($brand);
//         endforeach;

//         $moneys = get_the_terms( $post->ID, 'moneda');

//         foreach($moneys as $money):
//             $post->moneda = $money->name;
//         endforeach;


//         $my_vehicles = str_replace('[',"",$_COOKIE["garage"]);
//         $my_vehicles = str_replace(']',"",$my_vehicles);
//         $my_vehicles = explode(',',$my_vehicles);

//         $post->added = false;

//         if(reset($my_vehicles) != "" && count($my_vehicles) > 0 ){
//             foreach ($my_vehicles as $key => $value) {
//                 if($value == $post->ID){
//                     $post->added = true;
//                 }
//             }
//         }


//         $post->link = get_permalink($post->ID);
//     }
//     return $posts->posts;
// }