<?php
/**
 * Plugin Name: Modulo de Cotizador
 * Description: Administra taza de interes para cotizador
 * Version: 1.0
 * Author: Milkncookies <@mnc.gt>
 * Author URI: mailto:@mnc.gt
 * License
 */


function cotizador($moneda, $valor, $enganche, $plazo){


    $tasa = 0;
    if(strtoupper($moneda) == "Q"){
        $tasa = getTasaQuetzal($enganche);
    }else{
        $tasa = getTasaDolar($enganche);
    }


    $valor_a_financiar = $valor * (1 - $enganche);
    $interes_mensual = ($tasa / 12) * $valor_a_financiar;
    $cuota_nivelada_mensual = round ( pmt( $tasa*100, $plazo, $valor_a_financiar  ), 2) - $interes_mensual ;
    $cuoata_mensual = $cuota_nivelada_mensual + $interes_mensual;

    return array(
        "tasa" => $tasa,
        "valor_del_enganche" => number_format( $valor * $enganche, 2, '.', ','),
        "valor_a_financiar" => number_format( $valor_a_financiar, 2, '.', ','),
        "interes_mensual" => $interes_mensual,
        "cuota_nivelada_mensual" => number_format( $cuota_nivelada_mensual, 2, '.', ','),
        "cuoata_mensual" => number_format( $cuoata_mensual, 2, '.', ','),
    );

}

function getTasaQuetzal($enganche){
    $tasa = 0;
    switch ($enganche) {
        case 0.5:
            // $tasa = 0.0765;
            $tasa = 0.075;
            break;
        case 0.4:
            // $tasa = 0.079;
            $tasa = 0.075;
            break;
        case 0.3:
            // $tasa = 0.079;
            $tasa = 0.075;
            break;
        case 0.2:
            // $tasa = 0.0814;
            $tasa = 0.075;
            break;
        case 0.15:
            // $tasa = 0.083;
            $tasa = 0.075;
            break;
        case 0:
            // $tasa = 0.17;
            $tasa = 0.075;
            break;
    }
    return $tasa;
}

function getTasaDolar($enganche){
    $tasa = 0;
    switch ($enganche) {
        case 0.5:
            // $tasa = 0.0699;
            $tasa = 0.065;
            break;
        case 0.4:
            // $tasa = 0.0725;
            $tasa = 0.065;
            break;
        case 0.3:
            // $tasa = 0.0725;
            $tasa = 0.065;
            break;
        case 0.2:
            // $tasa = 0.0735;
            $tasa = 0.065;
            break;
        case 0.15:
            // $tasa = 0.0745;
            $tasa = 0.065;
            break;
        case 0:
            // $tasa = 0.12;
            $tasa = 0.065;
            break;
    }
    return $tasa;
}

//FUNCTION EXCEL PMT
//INTEREST IN % NOT IN DECIMAL
function pmt($interest, $months, $loan) {
    // var_dump($interest);
    $months = $months;
    $interest = $interest / 1200;
    $amount = $interest * -$loan * pow((1 + $interest), $months) / (1 - pow((1 + $interest), $months));
    return $amount;
}
