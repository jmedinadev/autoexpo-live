��          \      �       �   8   �             �   %          #  !   ;  J  ]  F   �     �              *     H  !   `                                       Highly Customisable Responsive Menu Plugin for WordPress Peter Featherstone Responsive Menu Responsive Menu requires PHP 5.4 or higher to function and has therefore been automatically disabled. You are still on %s.%sPlease speak to your webhost about upgrading your PHP version. For more information please visit %s https://peterfeatherstone.com https://responsive.menu responsive, menu, responsive menu PO-Revision-Date: 2020-04-02 11:39:10+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha
Language: es
Project-Id-Version: Plugins - Responsive Menu &#8211; Create Mobile-Friendly Menu - Stable (latest release)
 Plugin para WordPress de menús adaptables y altamente personalizables Peter Featherstone Responsive Menu Para funcionar, Responsive Menu necesita PHP 5.4 o superior y, por tanto, ha sido desactivado automáticamente. Todavía estás en la versión %s.%sPor favor, habla con tu alojamiento web sobre la actualización de tu versión de PHP. Para más información, por favor, visita %s https://peterfeatherstone.com https://responsive.menu adaptable, menú, menú adaptable 